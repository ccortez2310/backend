<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Slider extends BaseController
{

	private $path = 'uploads/sliders/';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Slider_model');
	}

	public function index()
	{
		$data = ['sliders' => $this->Slider_model->getSliders()];
		$this->loadViews('sliders/sliders', $data);
	}

	public function getSliderByID()
	{
		$id = $this->input->post("idSlider");
		$slider = $this->Slider_model->getSliderByID($id);
		echo json_encode($slider);

	}

	public function changeState()
	{
		$id = $this->input->post("idSlider");
		$data = [
			"estado" => $this->input->post("estadoSlider")
		];
		if ($this->Slider_model->changeState($id, $data)) {
			echo "ok";
		} else {
			echo "error";
		}
	}

	public function save()
	{
		$titulo = $this->input->post('titulo');
		$descripcion = $this->input->post('descripcion');
		$resizePortada = $this->input->post('resizePortada');

		if (isset($resizePortada) && $resizePortada != "") {
			$portada = upload_image('portada', $this->path, true, 1300, 700);
		} else {
			$portada = upload_image('portada', $this->path);
		}

		$data = [
			"titulo" => $titulo,
			"descripcion" => $descripcion,
			"portada" => $portada,
			"estado" => 1
		];

		if ($this->Slider_model->save($data)) {
			$this->session->set_flashdata("success", "El Slider ha sido guardado con Éxito");
			redirect(base_url('sliders'));
		} else {
			$this->session->set_flashdata("error", "Ha ocurrido un error al intentar guardar");
			redirect(base_url('sliders'));
		}

	}


	public function update()
	{
		$id = $this->input->post('idSlider');
		$titulo = $this->input->post('titulo');
		$descripcion = $this->input->post('descripcion');
		$portadaActual = $this->input->post('portadaActual');
		$resizePortada = $this->input->post('resizePortada');

		if (isset($_FILES["portada"]["tmp_name"]) && !empty($_FILES["portada"]["tmp_name"])) {
			unlink('./' . $portadaActual);
			if(isset($resizePortada) && $resizePortada != ""){
				$portada = upload_image('portada', $this->path, true, 1300, 700);
			} else {
				$portada = upload_image('portada', $this->path);
			}

		} else {
			$portada = $portadaActual;
		}

		$data = [
			"titulo" => $titulo,
			"descripcion" => $descripcion,
			"portada" => $portada
		];

		if ($this->Slider_model->update($id, $data)) {
			$this->session->set_flashdata("success", "El Slider ha sido actualizado con Éxito");
			redirect(base_url('sliders'));
		} else {
			$this->session->set_flashdata("error", "Ha ocurrido un error al intentar guardar");
			redirect(base_url('sliders'));
		}

	}

	public function delete()
	{
		$id = $this->input->post("idSlider");
		$img = $this->input->post("imgSlider");

		#Eliminamos la imagen del slider
		unlink("./" . $img);

		if ($this->Slider_model->delete($id)) {
			echo json_encode("ok");
		} else {
			echo json_encode("error");
		}

	}

}
