<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Dashboard extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Dashboard_model');
	}

	public function index()
	{
		$data = [
			"dashboard" => ""
		];
		$this->loadViews("dashboard/dashboard", $data);
	}

}
