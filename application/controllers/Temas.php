<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Temas extends BaseController
{

	private $path = 'uploads/temas/';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Temas_model');
		$this->load->model('Headers_model');
	}

	public function index()
	{
		$data = ['temas' => $this->Temas_model->getTemas()];
		$this->loadViews('temas/temas', $data);
	}

	public function getTemaByID()
	{
		$id = $this->input->post("idTema");
		$tema = $this->Temas_model->getTemaByID($id);
		$cabecera = $this->Headers_model->getHeader($tema->slug);

		$data = [
			"id" => $tema->id,
			"tema" => $tema->tema,
			"descripcion" => $tema->descripcion,
			"slug" => $tema->slug,
			"portada" => $tema->portada,
			"idHeader" => $cabecera->id
		];

		echo json_encode($data);
	}

	public function changeState()
	{
		$id = $this->input->post("idTema");
		$data = [
			"estado" => $this->input->post("estadoTema")
		];
		if ($this->Temas_model->changeState($id, $data)) {
			echo "ok";
		} else {
			echo "error";
		}
	}

	public function save()
	{

		$tema = $this->input->post('tema');
		$slug = $this->input->post('slug');
		$descripcion = $this->input->post('descripcion');
		$resizePortada = $this->input->post('resizePortada');

		if (isset($resizePortada) && $resizePortada != "") {
			$portada = upload_image('portada', $this->path, true, 700, 420);
		} else {
			$portada = upload_image('portada', $this->path);
		}

		#arreglo que llenara la tabla temas
		$data = [
			"tema" => $tema,
			"slug" => $slug,
			"descripcion" => $descripcion,
			"portada" => $portada,
			"estado" => 1
		];

		#para las cabeceras
		$headerData = [
			"titulo" => $tema,
			"ruta" => $slug,
			"descripcion" => $descripcion,
			"portada" => $portada
		];

		#guardamos las cabeceras
		$this->Headers_model->save($headerData);

		if ($this->Temas_model->save($data)) {
			$this->session->set_flashdata("success", "El Tema ha sido guardado con Éxito");
			redirect(base_url('temas'));
		} else {
			$this->session->set_flashdata("error", "Ha ocurrido un error al intentar guardar");
			redirect(base_url('temas'));
		}

	}


	public function update()
	{
		$id = $this->input->post('idTema');
		$idHeader = $this->input->post('idHeader');
		$tema = $this->input->post('tema');
		$slug = $this->input->post('slug');
		$descripcion = $this->input->post('descripcion');
		$portadaActual = $this->input->post('portadaActual');
		$resizePortada = $this->input->post('resizePortada');

		if (isset($_FILES["portada"]["tmp_name"]) && !empty($_FILES["portada"]["tmp_name"])) {
			unlink('./' . $portadaActual);
			if (isset($resizePortada) && $resizePortada != "") {
				$portada = upload_image('portada', $this->path, true, 700, 420);
			} else {
				$portada = upload_image('portada', $this->path);
			}

		} else {
			$portada = $portadaActual;
		}

		#arreglo que llenara la tabla temas
		$data = [
			"tema" => $tema,
			"slug" => $slug,
			"descripcion" => $descripcion,
			"portada" => $portada
		];

		#para las cabeceras
		$headerData = [
			"titulo" => $tema,
			"ruta" => $slug,
			"descripcion" => $descripcion,
			"portada" => $portada
		];

		#actualizamos las cabeceras
		$this->Headers_model->update($idHeader,$headerData);

		if ($this->Temas_model->update($id, $data)) {
			$this->session->set_flashdata("success", "El Tema ha sido actualizado con Éxito");
			redirect(base_url('temas'));
		} else {
			$this->session->set_flashdata("error", "Ha ocurrido un error al intentar guardar");
			redirect(base_url('temas'));
		}

	}

	public function delete()
	{
		$id = $this->input->post("idTema");
		$img = $this->input->post("imgTema");
		$slug = $this->input->post("slug");

		#Eliminamos la imagen del slider
		unlink("./" . $img);

		#Eliminamos los datos de la cabecera
		$this->Headers_model->delete($slug);

		if ($this->Temas_model->delete($id)) {
			echo json_encode("ok");
		} else {
			echo json_encode("error");
		}

	}

}
