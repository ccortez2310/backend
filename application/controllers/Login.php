<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model');
	}

	public function index()
	{
		$this->load->view("login");
	}

	public function login()
	{
		$email = $this->input->post("email");
		$pass = $this->input->post("pass");
		$res = $this->Users_model->login($email, $pass);
		if (!$res) {
			$this->session->set_flashdata("error", "El usuario y/o contraseña son incorrectos");
			redirect(base_url());
		} else {
			$data = [
				"logged_in" => true,
				"nombre" => $res->nombre,
				"foto" => $res->foto,
				"perfil" => $res->perfil,
				"email" => $res->email
			];

			$this->session->set_userdata($data);
			redirect(base_url() . "dashboard");
		}

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

}
