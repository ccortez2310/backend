<?php
require APPPATH . 'libraries/BaseController.php';

defined('BASEPATH') OR exit('No direct script access allowed');

class Podcasts extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Podcasts_model');
		
	}

	public function index()
	{
		$data['podcastsList'] = $this->Podcasts_model->get_all();
		
		
		$this->loadViews('podcasts/podcasts', $data);
	}

	public function New()
	{
		$this->loadViews('podcasts/new');
	}

	public function Insert($data)
	{
		return $this->db->insert('podcasts', $data);
	}
}
