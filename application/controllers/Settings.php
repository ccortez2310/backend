<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Settings extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Template_model');
	}

	public function index()
	{
		$data = ['template' => $this->Template_model->getTemplateData()];
		$this->loadViews('settings/settings', $data);
	}

	public function save()
	{
		$destinationPath = 'public/img/template/';

		$id = $this->input->post('id');
		$iconoActual = $this->input->post('iconoActual');
		$logoActual = $this->input->post('logoActual');
		$resizeLogo = $this->input->post('resizeLogo');
		$portadaActual = $this->input->post('portadaActual');
		$resizePortada = $this->input->post('resizePortada');
		$email = $this->input->post('email');
		$telefono = $this->input->post('telefono');
		$redes = $this->input->post('redes');

		#Para subir el icono
		if (isset($_FILES["icono"]["tmp_name"]) && !empty($_FILES["icono"]["tmp_name"])) {
			unlink('./' . $iconoActual);
			$icono = upload_image('icono', $destinationPath, true, 64, 64);
		} else {
			$icono = $iconoActual;
		}

		#Para subir el logo
		if (isset($_FILES["logo"]["tmp_name"]) && !empty($_FILES["logo"]["tmp_name"])) {
			unlink('./' . $logoActual);
			if(isset($resizeLogo) && $resizeLogo != ""){
				$logo = upload_image('logo', $destinationPath, true, 500, 200);
			} else {
				$logo = upload_image('logo', $destinationPath);
			}

		} else {
			$logo = $logoActual;
		}

		#Para subir la portada
		if (isset($_FILES["portada"]["tmp_name"]) && !empty($_FILES["portada"]["tmp_name"])) {
			unlink('./' . $portadaActual);
			if(isset($resizePortada) && $resizePortada != ""){
				$portada = upload_image('portada', $destinationPath, true, 700, 420);
			} else {
				$portada = upload_image('portada', $destinationPath);
			}

		} else {
			$portada = $portadaActual;
		}

		#creamos el array de datos
		$data = [
			"icono" => $icono,
			"logo" => $logo,
			"portada" => $portada,
			"email" => $email,
			"telefono" => $telefono,
			"redesSociales" => $redes
		];


		if ($this->Template_model->save($id, $data)) {
			$this->session->set_flashdata("success", "Datos guardados con Éxito");
			redirect(base_url('settings'));
		} else {
			$this->session->set_flashdata("error", "Ha ocurrido un error al intentar guardar");
			redirect(base_url('settings'));
		}

	}


}
