<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Documentos extends BaseController
{
    private $path = 'uploads/documentos/';

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('Documentos_model');
    }

    public function index()
    {
        $data = ['docs' => $this->Documentos_model->getDocs()];
        $this->loadViews('documentos/documentos', $data);
    }

    public function getDocById()
    {
        $id = $this->input->post("idDoc");
        $doc = $this->Documentos_model->getDocByID($id);
        echo json_encode($doc);
        var_dump($doc);
    }

    /*public function changeDocState()
    {
        $id = $this->input->post("idDoc");
        $data = [
            "estado" => $this->input->post("estadoDoc")
        ];
        if ($this->Documentos_model->changeDocState($id, $data)) {
            echo "ok";
        } else {
            echo "error";
        }
    }*/

    public function saveDoc()
    {
        $titulo = $this->input->post('titulo');
        $descripcion = $this->input->post('descripcion');

        trim($titulo);
        strtolower($titulo);
        str_replace(" ", "-", $titulo);

        $doc = upload_file('docFile', $this->path, $titulo);

        $data = [
            "titulo" => $titulo,
            "descripcion" => $descripcion,
            "peso" => $doc['weight'],
            "path" => $doc['path'],
            "id_categoria" => 6,
            "estado" => 1
        ];

        if ($this->Documentos_model->saveDoc($data)) {
            $this->session->set_flashdata("success", "El documento ha sido agregado con éxito");
            redirect(base_url('documentos'));
        } else {
            $this->session->set_flashdata("error", "Ha ocurrido un error al intentar guardar");
            redirect(base_url('documentos'));
        }
    }


    public function deleteDoc()
    {
        $id = $this->input->post("idDoc");
		$docFile = $this->input->post("docFile");
        unlink("./" . $docFile);

        if ($this->Documentos_model->deleteDoc($id)) {
            echo json_encode("ok");
        } else {
            echo json_encode("error");
        }
    }
    
}
