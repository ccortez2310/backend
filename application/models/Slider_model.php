<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class Slider_model extends CI_Model
{
	public function getSliders()
	{
		return $this->db->get('sliders')->result();
	}
	public function getSliderByID($id)
	{
		$this->db->where("id", $id);
		return $this->db->get("sliders")->row();
	}

	public function changeState($id, $data)
	{
		$this->db->where("id", $id);
		return $this->db->update("sliders", $data);
	}

	public function save($data)
	{
		return $this->db->insert('sliders', $data);
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update('sliders', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('sliders');
	}

}
