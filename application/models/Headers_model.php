<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class Headers_model extends CI_Model
{

	public function getHeader($ruta)
	{
		$this->db->where("ruta", $ruta);
		return $this->db->get("cabeceras")->row();
	}

	public function save($data)
	{
		$this->db->insert("cabeceras", $data);
	}

	public function update($id, $data)
	{
		$this->db->where("id", $id);
		$this->db->update("cabeceras", $data);
	}

	public function delete($ruta)
	{
		$this->db->where("ruta", $ruta);
		$this->db->delete("cabeceras");
	}

}
