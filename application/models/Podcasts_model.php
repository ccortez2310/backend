<?php


class Podcasts_model extends CI_Model
{
    protected $table = 'podcasts';

    public function get_all()
    {
        return $this->db->get($this->table)
            ->result();
    }
}
