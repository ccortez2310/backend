<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Documentos_model extends CI_Model
{
    public function getDocs()
    {
        return $this->db->get('documentos')->result();
    }

    public function getDocByID($id)
    {
        $this->db->where("id", $id);
        return $this->db->get("documentos")->row();
    }

    public function changeDocState($id, $data)
    {
        $this->db->where("id", $id);
        return $this->db->update("documentos", $data);
    }

    public function saveDoc($data)
    {
        return $this->db->insert('documentos', $data);
    }

    public function updateDoc($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('documentos', $data);
    }

    public function deleteDoc($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('documentos');
    }
}
