<?php

defined('BASEPATH') or exit ('No direct script access allowed');

class Users_model extends CI_Model
{
	public function login($email, $pass)
	{
		$this->db->select("*");
		$this->db->from("usuarios");
		$this->db->where("email", $email);
		$this->db->where("password", $pass);
		$res = $this->db->get();
		if ($res->num_rows() > 0) {
			return $res->row();
		} else {
			return false;
		}
	}

}
