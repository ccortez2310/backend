<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class Temas_model extends CI_Model
{
	public function getTemas()
	{
		return $this->db->get('temas')->result();
	}

	public function getTemaByID($id)
	{
		$this->db->where("id", $id);
		return $this->db->get("temas")->row();
	}

	public function changeState($id, $data)
	{
		$this->db->where("id", $id);
		return $this->db->update("temas", $data);
	}

	public function save($data)
	{
		return $this->db->insert('temas', $data);
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update('temas', $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('temas');
	}
}
