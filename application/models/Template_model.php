<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class Template_model extends CI_Model
{
	public function getTemplateData()
	{
		return $this->db->get('plantilla', 1)->row();
	}

	public function save($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update('plantilla', $data);
	}

}
