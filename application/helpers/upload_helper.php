<?php

/**
 * Upload Helper
 *
 * Subir archivos al servidor
 *
 * @author        Carlos Cortez
 * @version     1.0
 */

/**
 * Upload Image Files
 *
 * Subir archivos de imagen al servidor
 *
 * @access  public
 * @param string $file
 * @param string $destinationPath
 * @return string $output
 */

if (!function_exists('upload_image')) {

	function upload_image($file, $destinationPath, $isResizable = FALSE, $width = NULL, $height= NULL)
	{
		$ci =& get_instance();

		$ci->load->library('upload');

		$titulo = md5(rand(100, 200));
		$extension = explode('.', $_FILES[$file]['name']);
		$archivo = $titulo . '.' . $extension[1];

		$config['upload_path'] = $destinationPath;
		$config['allowed_types'] = 'jpg|jpeg|png|ico';
		$config['file_name'] = $archivo;

		$ci->upload->initialize($config);

		if (!$ci->upload->do_upload($file)) {
			echo $ci->upload->display_errors();
		} else {

			$data = $ci->upload->data();

			if(!$isResizable) {
				$output =  $destinationPath . $data['file_name'];
			} else {
				$config['image_library'] = 'gd2';
				$config['source_image'] = $destinationPath . $data['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['quality'] = '90%';
				$config['width'] = $width;
				$config['height'] = $height;
				$config['new_image'] = $destinationPath . $data['file_name'];
				$ci->load->library('image_lib', $config);
				$ci->image_lib->resize();
				$output =  $destinationPath . $data['file_name'];
			}

			return $output;
		}

	}
}

if (!function_exists('upload_file')) {

	function upload_file($file, $destinationPath, $name)
	{
		$ci = &get_instance();

		$ci->load->library('upload');

		$extension = explode('.', $_FILES[$file]['name']);
		$archivo = $name . '.' . $extension[1];

		$config['upload_path'] = $destinationPath;
		$config['allowed_types'] = 'png|pdf|doc|docx';
		$config['file_name'] = $archivo;

		$ci->upload->initialize($config);

		if (!$ci->upload->do_upload($file)) {
			echo $ci->upload->display_errors();
		} else {
			$data = $ci->upload->data();
			$result = [
				"path" => $destinationPath.$data['file_name'],
				"weight" => $data["file_size"].' KB'
			];
			

			return $result;
		}
	}
}