<?php

defined('BASEPATH') or exit ('No direct script access allowed');


class BaseController extends CI_Controller
{
	/**
	 * This function used to load views
	 * @param string $viewName
	 * @param null $pageInfo
	 * @return void {null} $result : null $result : null
	 */
	function loadViews($viewName = "",  $pageInfo = NULL)
	{

		$this->load->model('Template_model');

		$templateInfo = [
			"template" => $this->Template_model->getTemplateData()
		];

		$this->load->view('template/header', $templateInfo);
		$this->load->view('template/nav', $templateInfo);
		$this->load->view($viewName, $pageInfo);
		$this->load->view('template/footer');
	}

}
