<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Portal Recursos | Iniciar Sesión</title>
	<link rel="icon" href="<?=base_url() ?>public/img/template/icon.png">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url() ?>public/dist/css/adminlte.min.css">
	<!--    css personalizados-->
	<link rel="stylesheet" href="<?=base_url() ?>public/css/plantilla.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="Javascript:void(0)"><b>Portal de Recursos</b></a>
	</div>
	<!-- /.login-logo -->
	<div class="card">
		<div class="card-body login-card-body">
			<p class="login-box-msg">Iniciar Sesión</p>

			<form action="<?= base_url('login/login')?>" method="post">
				<div class="input-group mb-3">
					<input type="email" class="form-control" name="email" placeholder="Email" required>
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-envelope"></span>
						</div>
					</div>
				</div>
				<div class="input-group mb-3">
					<input type="password" class="form-control" name="pass" placeholder="Contraseña" required>
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-lock"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<button type="submit" class="btn btn-blue btn-block">Iniciar Sesión</button>
					</div>
					<!-- /.col -->
				</div>
				<?php if($this->session->flashdata("error")): ?>
					<div class="row py-3">
						<div class="col-12">
							<div class="alert alert-danger">
								<?php echo $this->session->flashdata("error") ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</form>
		</div>
	</div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?=base_url() ?>public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url() ?>public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>

