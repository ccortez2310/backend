<?php if ($this->session->flashdata('success')): ?>
	<script type="text/javascript">
		notie.alert({
			type: 1,
			text: "<?php echo $this->session->flashdata("success") ?>",
			time: 3
		})
	</script>
<?php endif; ?>
<div class="content-wrapper">
	<!--BreadCrumbs-->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Configuración del Sitio</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Inicio</a></li>
						<li class="breadcrumb-item active">Ajustes</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!--Contenido -->
	<div class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header ">
					<h5 class="mt-1 float-left">
						<i class="fas fa-cogs"></i> Ajustes del Sitio
					</h5>
				</div>
				<div class="card-body">
					<form action="<?= base_url('Settings/save') ?>" enctype="multipart/form-data" method="post">
						<div class="row">
							<div class="col-lg-6 col-md-12">
								<div class="card">
									<div class="card-header">
										<h6><i class="fas fa-images"></i>
											Icono, Logo y Portada Principal del Sitio</h6>
									</div>
									<div class="card-body">
										<input type="hidden" name="id" value="<?php echo $template->id ?>">
										<div class="row">
											<div class="col-12">
												<div class="col-lg-12">
													<div class="form-group my-2 text-center">
														<div class="btn btn-default btn-file mb-3">
															<i class="fas fa-paperclip"></i> Adjuntar Icono
															<input type="file" name="icono" id="icono">
															<input type="hidden" name="iconoActual"
																   value="<?php echo $template->icono ?>">
														</div>
														<br>
														<div class="bg-gray">
															<img src="<?= base_url() . $template->icono ?>" alt="Icono"
																 class="img-fluid py-2 prevIcono">
														</div>
														<p class="help-block small mt-3">Dimensiones: 64px * 64px |
															Peso Max. 1MB | Formato: JPG, PNG o ICO</p>
													</div>
													<hr>
													<div class="form-group my-2 text-center">
														<div class="btn btn-default btn-file mb-3">
															<i class="fas fa-paperclip"></i> Adjuntar Logo
															<input type="file" name="logo" id="logo">
															<input type="hidden" name="logoActual"
																   value="<?php echo $template->logo ?>">
														</div>
														<br>
														<div class="bg-gray">
															<img src="<?= base_url() . $template->logo ?>" alt="Logo"
																 class="img-fluid py-2 prevLogo">
														</div>
														<p class="help-block small mt-3">Dimensiones: 500px * 200px |
															Peso Max. 2MB | Formato: JPG o PNG</p>
														<div class="custom-control custom-switch">
															<input class="custom-control-input" type="checkbox"
																   name="resizeLogo" id="resizeLogo" value="Si">
															<label for="resizeLogo" class="custom-control-label">Forzar
																Redimensionado de Imagen</label>
														</div>
													</div>
													<hr>
													<div class="form-group my-2 text-center">
														<div class="btn btn-default btn-file mb-3">
															<i class="fas fa-paperclip"></i> Adjuntar Imagen Portada
															<input type="file" name="portada" id="portada">
															<input type="hidden" name="portadaActual"
																   value="<?php echo $template->portada ?>">
														</div>
														<br>
														<img src="<?= base_url() . $template->portada ?>" alt="Portada"
															 class="img-fluid py-2 prevPortada">
														<p class="help-block small mt-3">Dimensiones: 700px * 420px |
															Peso Max. 2MB | Formato: JPG o PNG</p>
														<div class="custom-control custom-switch">
															<input class="custom-control-input" type="checkbox"
																   name="resizePortada" id="resizePortada" value="Si">
															<label for="resizePortada"
																   class="custom-control-label">Forzar
																Redimensionado de Imagen</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-12">
								<div class="card">
									<div class="card-header">
										<h6>
											<i class="fas fa-list-alt"></i> Email, Télefono y Redes Sociales
										</h6>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<div class="input-group mb-3">
														<div class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-envelope"></i>
															</span>
														</div>
														<input type="email" class="form-control" name="email"
															   value="<?php echo $template->email ?>"
															   placeholder="Email">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<div class="input-group mb-3">
														<div class="input-group-prepend">
															<span class="input-group-text">
																<i class="fas fa-phone"></i>
															</span>
														</div>
														<input type="tel" class="form-control" name="telefono"
															   value="<?php echo $template->telefono ?>"
															   placeholder="Teléfono">
													</div>
												</div>
											</div>
										</div>
										<hr>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<label>Redes Sociales</label>
													<div class="row">
														<div class="col-5">
															<div class="input-group mb-3">
																<div class="input-group-prepend">
																	<span class="input-group-text">Red</span>
																</div>
																<select class="form-control" id="icono_red">
																	<option value="fab fa-facebook-square fa-2x, #1475E0">
																		Facebook
																	</option>
																	<option value="fab fa-instagram fa-2x, #B18768">
																		Instagram
																	</option>
																	<option value="fab fa-twitter-square fa-2x, #00A6FF">
																		Twitter
																	</option>
																	<option value="fab fa-youtube-square fa-2x, #F95F62">
																		Youtube
																	</option>
																	<option value="fab fa-snapchat-square fa-2x, #FF9052">
																		Snapchat
																	</option>
																	<option value="fab fa-linkedin fa-2x, #0E76A8">
																		Linkedin
																	</option>
																</select>
															</div>
														</div>
														<div class="col-7">
															<div class="input-group mb-3 fa-linke">
																<div class="input-group-prepend">
																	<span class="input-group-text">Url</span>
																</div>
																<input type="text" class="form-control" id="url_red">
																<div class="input-group-append">
																	<button type="button"
																			class="btn btn-primary bg-indigo agregarRed">
																		<i class="fas fa-plus-square"></i>
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="row listadoRedes">
											<input type="hidden" value='<?php echo $template->redesSociales ?>'
												   name="redes" id="listaRed">

											<?php
											$redes = json_decode($template->redesSociales);
											foreach ($redes as $red) :
												?>
												<div class="col-12">
													<div class="callout callout-indigo">
														<div class="row">
															<div class="col-11 d-flex align-items-center">
																<a type="button"
																   style="color: <?php echo $red->color ?>">
																	<i class="<?php echo $red->icono ?>"></i>
																</a>
																<span class="ml-md-2 ml-2 ml-sm-2"><?php echo $red->url ?></span>
															</div>
															<div class="col-1">
																<button type="button"
																		class="btn btn-outline-danger btn-xs eliminarRed"
																		red="<?php echo $red->icono ?>"
																		url="<?php echo $red->url ?>"
																		title="Haga click para eliminar">
																	<i class="fas fa-trash"></i>
																</button>
															</div>
														</div>
													</div>
												</div>

											<?php endforeach; ?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-12 text-right">
								<button type="submit" class="btn btn-outline-primary">
									<i class="fas fa-save"></i> Guardar Cambios
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?= base_url() ?>public/js/settings.js"></script>
