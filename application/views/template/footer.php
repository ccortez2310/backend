<!-- Main Footer -->
<footer class="main-footer">
	<!-- Default to the left -->
	<strong>&copy; <?php echo date('Y'); ?> <a href="<?= base_url() ?>">Portal de Recursos </a>.</strong>
	Desarrollado por NGS Tecnology.
</footer>

</div><!-- End Wrapper -->


<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- AdminLTE App -->
<script src="<?= base_url() ?>public/dist/js/adminlte.min.js"></script>

<!-- Moment -->
<script src="<?= base_url() ?>public/plugins/moment/moment.min.js"></script>
<script src="<?= base_url() ?>public/plugins/moment/moment-with-locales.min.js"></script>

<!-- TOASTR-->
<script src="<?= base_url() ?>public/plugins/toastr/toastr.min.js"></script>


<!-- SUMMERNOTE-->
<script src="<?= base_url() ?>public/plugins/summernote/summernote.js"></script>

<!-- TAGSINPUT -->
<script src="<?= base_url() ?>public/plugins/tagsinput/tagsinput.js"></script>

<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>public/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- SweetAlert 2 https://sweetalert2.github.io/-->
<script src="<?= base_url() ?>public/plugins/sweetalert2/sweetalert2.all.js"></script>

<script src="<?= base_url() ?>public/js/dtes.js"></script>
<script src="<?= base_url() ?>public/js/plantilla.js"></script>

</body>
</html>
