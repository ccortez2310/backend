<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark navbar-primary">

	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>
	</ul>
	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">
		<li class="nav-item mt-1">
			<span class="dateday" id="dateday"></span>
			<span class="dateday" id="datedays"></span>
		</li>
		<li class="nav-item ml-2">
			<span class="datetime" id="datetime"></span>
		</li>
		<li class="nav-item ml-5">
			<a class="nav-link" href="<?= base_url('login/logout')?>"><i
						class="fas fa-sign-out-alt"></i> Salir</a>
		</li>
	</ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-blue elevation-4">
	<!-- Brand Logo -->
	<a href="<?= base_url('dashboard') ?>" class="brand-link navbar-primary">
		<img src="<?= base_url().$template->logo ?>" alt="Portal de Recursos" class="ml-4 brand-image"
			 style="opacity: 1">
		<span class="brand-text font-weight-light">&nbsp;</span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<?php if(empty($this->session->userdata("foto"))) { ?>
					<img src="<?= base_url() ?>public/img/usuarios/default/anonymous.png" class="img-circle elevation-2"
						 alt="usuario">
				<?php } else { ?>
					<img src="<?= base_url().$this->session->userdata("foto") ?>" class="img-circle elevation-2"
						 alt="usuario">
				<?php } ?>
			</div>
			<div class="info">
				<a href="javascript:void(0)" class="d-block"><?php echo $this->session->userdata("nombre"); ?></a>
			</div>
		</div>
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
					<a href="<?= base_url('dashboard')?>" class="nav-link">
						<i class="nav-icon fas fa-home"></i>
						<p>
							Inicio
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('settings')?>" class="nav-link">
						<i class="nav-icon fas fa-cogs"></i>
						<p>
							Configuración del Sitio
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('sliders')?>" class="nav-link">
						<i class="nav-icon fas fa-image"></i>
						<p>
							Sliders
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('temas')?>" class="nav-link">
						<i class="nav-icon fas fa-list-alt"></i>
						<p>
							Temas de Interés
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('videos')?>" class="nav-link">
						<i class="nav-icon fas fa-video"></i>
						<p>
							Videos
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('podcasts')?>" class="nav-link">
						<i class="nav-icon fas fa-podcast"></i>
						<p>
							Podcast
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('images')?>" class="nav-link">
						<i class="nav-icon fas fa-images"></i>
						<p>
							Imagenes
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('documentos')?>" class="nav-link">
						<i class="nav-icon fas fa-archive"></i>
						<p>
							Documentos
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('users')?>" class="nav-link">
						<i class="nav-icon fas fa-user-cog"></i>
						<p>
							Usuarios
						</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('help')?>" class="nav-link">
						<i class="nav-icon fas fa-question">  </i>
						<p>
							Ayuda
						</p>
					</a>
				</li>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>

