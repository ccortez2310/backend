<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Portal Recursos | Panel de Control</title>

	<link rel="icon" href="<?=base_url().$template->icono ?>">

	<!-- REQUIRED CSS -->

	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/fontawesome-free/css/all.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

	<!-- JQUERY UI -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/jquery-ui/jquery-ui.min.css">

	<!-- BOOTSTRAP DATEPICKER -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/datepicker/css/bootstrap-datepicker.min.css">

	<!-- TOASTR -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/toastr/toastr.min.css">

	<!-- NOTIE -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/notie/notie.css">

	<!-- SUMMERNOTE -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/summernote/summernote.css">

	<!-- TAGSINPUT -->
	<link rel="stylesheet" href="<?=base_url() ?>public/plugins/tagsinput/tagsinput.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url() ?>public/dist/css/adminlte.min.css">
	<!-- Spinner -->
	<link rel="stylesheet" href="<?=base_url() ?>public/dist/css/spinner.css">

	<!-- Google Font: Source Sans Pro -->
<!--	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">-->

	<!--    css personalizados-->
	<link rel="stylesheet" href="<?=base_url() ?>public/css/plantilla.css">

	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
	<script src="<?=base_url() ?>public/plugins/jquery/jquery.min.js"></script>
	<!-- JQUERY UI-->
	<script src="<?=base_url() ?>public/plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?=base_url() ?>public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- NOTIE-->
	<script src="<?=base_url() ?>public/plugins/notie/notie.js"></script>

</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">

<div class="preloader">
	<div class="loader">
		<div class="loader__figure"></div>
		<p class="loader__label">Portal de Recursos</p>
	</div>
</div>


<div class="wrapper">

