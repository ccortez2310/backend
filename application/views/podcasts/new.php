<?php 

if( isset($formData)){
    var_dump($formData);
}

?>
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="fas fa-plus-circle"></i> Nuevo podcast</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('podcasts/podcasts') ?>">Podcasts</a></li>
                        <li class="breadcrumb-item active">Nuevo</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class=" card">
                <div class="card-header">
                    <h5 class="card-tittle h5">
                        Complete con los siguientes datos
                    </h5>
                </div>
                <!-- form start -->
                <form role="form">
                    <div class="card-body">
                        <input type="number" hidden value="1" name="id_categoria">
                        <div class="form-group">
                            <label for="titulo">Titulo</label>
                            <input type="titulo" class="form-control" name="titulo" id="titulo" placeholder="Ingrese el titulo del podcast">
                        </div>

                        <div class="form-group btn btn-default btn-file mb-3">
                            <i class="fas fa-image"></i> Poster
                            <input type="file" name="poster" id="poster">
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripcion</label>
                            <textarea type="descripcion" class="form-control" id="descripcion" placeholder=""></textarea>
                        </div>
                        <div class="btn btn-default btn-file mb-3">
                            <i class="fas fa-upload"></i> Podcast
                            <input type="file" name="path" id="path">
                        </div>
                        <br>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Aceptar</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>