<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="fas fa-podcast"></i> Podcasts</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Inicio</a></li>
                        <li class="breadcrumb-item active">Podcasts</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header ">
                    <div class="row">

                        <div class="col-5">
                            <h5 class="mt-1 float-left">
                                <a href="<?= base_url('podcasts/new') ?>"><i class="fas fa-plus-circle"></i> Agregar </a>
                            </h5>
                        </div>
                        <div class="col-7 float-right">
                            <input class="form-control" type="text" placeholder="Buscar" aria-label="Search">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body">
                                    <?php foreach ($podcastsList as $item) : ?>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="media row">
                                                    <img class="d-flex align-self-start mr-3" src="<?= base_url($item->poster) ?>" alt="" width="250">
                                                    <div class="media-body col">
                                                        <h5 class="mt-0 font-weight-bold">
                                                            <?= $item->titulo ?>
                                                            <small class="text-secondary text">
                                                                <i class="fa fa-link" aria-hidden="true"></i> <?= $item->slug ?>
                                                            </small>
                                                        </h5>
                                                        <p>
                                                            <small>

                                                                <?= $item->descripcion ?>
                                                            </small>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <audio src="<?= base_url($item->path) ?>" controls>
                                                            <p>Tu navegador no implementa el elemento audio.</p>
                                                        </audio>
                                                        <br>
                                                        <p>
                                                            <i>Ultima modificacion: <?= $item->fecha ?></i>
                                                        </p>
                                                        <p><?= $item->total_vistas ?> vistas, <?= $item->total_descargas ?> descargas</p>
                                                    </div>
                                                    <div>
                                                        <button type="button" class="btn btn-link btn-lg pull-right"><i class="fas fa-edit" aria-hidden="true"></i></button>
                                                        <button type="button" class="btn btn-link btn-lg pull-right" data-toggle="modal" data-target="#myModal"><i class="fas fa-trash" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                        </div>
                                        <div class="modal-body">
                                            ...
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>