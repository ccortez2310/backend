<?php if ($this->session->flashdata('success')): ?>
	<script type="text/javascript">
		//toastr.success("<?//php //echo $this->session->flashdata("success")?>//");
		notie.alert({
			type: 1,
			text: "<?php echo $this->session->flashdata("success") ?>",
			time: 3
		})
	</script>
<?php endif; ?>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Temas de Interés</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url() ?>">Inicio</a></li>
						<li class="breadcrumb-item active">Temas de Interés</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header ">
					<h5 class="mt-1 float-left">
						<i class="fas fa-list-alt"></i> Todos los Temas
					</h5>
					<button class="btn btn-outline-primary float-right" data-toggle="modal"
							data-target="#modalNuevoTema">
						<i class="fas fa-plus-circle"></i> Nuevo Tema
					</button>
				</div>
				<div class="card-body">
					<table class="table table-hover dt-responsive dtes tablaTemas">
						<thead>
						<tr>
							<th>#</th>
							<th>Tema</th>
							<th>Slug</th>
							<th>Descripción</th>
							<th>Imagen</th>
							<th>Estado</th>
							<th>Acciones</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($temas as $key => $value):

							if ($value->estado == 0) {
								$color = "btn-danger";
								$estado = 1;
								$texto = "Desactivado";
							} else {
								$color = "btn-success";
								$estado = 0;
								$texto = "Activado";
							}

							$btnEstado = '<button type="button" 
												  class="btn ' . $color . ' btn-xs btnActivar"
												  idTema = "' . $value->id . '" 
												  estadoTema="' . $estado . '">' . $texto . '</button>';

							?>
							<tr>
								<td><?php echo($key + 1) ?></td>
								<td><?php echo $value->tema ?></td>
								<td><?php echo $value->slug ?></td>
								<td><?php echo $value->descripcion ?></td>
								<td>
									<a data-toggle="modal" data-target="#tema<?php echo $value->id ?>">
										<img src="<?php echo $value->portada ?>"
											 alt="<?php echo $value->tema ?>"
											 class="img-thumbnail" width="100px">
									</a>
									<div class="modal fade" id="tema<?php echo $value->id ?>" tabindex="-1"
										 role="dialog">
										<button type="button" class="close mr-3 text-white" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
											<img src="<?php echo $value->portada ?>"
												 alt="<?php echo $value->tema ?>"
												 class="img-fluid rounded">
										</div>
									</div>
								</td>
								<td><?php echo $btnEstado ?></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-warning text-white btnEditarTema"
												idTema="<?php echo $value->id ?>"
												title="Editar"
												data-toggle="modal"
												data-target="#modalEditarTema">
											<i class="fas fa-edit"></i>
										</button>
										<button type="button" class="btn btn-danger btnEliminarTema"
												idTema="<?php echo $value->id ?>"
												slug="<?php echo $value->slug ?>"
												imgTema="<?php echo $value->portada ?>"
												title="Eliminar">
											<i class="fas fa-trash"></i>
										</button>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--==========================
MODAL NUEVO TEMA
============================-->
<div class="modal fade" id="modalNuevoTema" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?= base_url('Temas/save') ?>" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<i class="fa fa-plus-circle"></i>
						Agregar Tema
					</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="tema">Tema:</label>
						<input type="text" class="form-control tema" id="tema"
							   name="tema"
							   placeholder="Escriba el Tema" required>
					</div>
					<div class="form-group">
						<label for="slug">Slug:</label>
						<input type="text" class="form-control" id="slug"
							   name="slug" readonly>
					</div>
					<div class="form-group">
						<label for="descripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion" rows="3"
								  id="descripcion" placeholder="Escriba la descripción"></textarea>
					</div>
					<div class="form-group my-2 text-center">
						<div class="btn btn-default btn-file mb-3">
							<i class="fas fa-paperclip"></i> Adjuntar Imagen del portada
							<input type="file" name="portada" id="portada" class="portada" required>
						</div>
						<br>
						<img src="<?= base_url() . 'uploads/temas/default/default.jpg' ?>" alt="Portada"
							 class="img-fluid py-2 prevPortada">
						<p class="help-block small mt-3">Dimensiones: 700px * 420px |
							Peso Max. 2MB | Formato: JPG o PNG</p>
						<div class="custom-control custom-switch">
							<input class="custom-control-input" type="checkbox"
								   name="resizePortada" id="resizePortada" value="Si">
							<label for="resizePortada"
								   class="custom-control-label">Forzar
								Redimensionado de Imagen</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
						<i class="fas fa-times-circle"></i> Cerrar
					</button>
					<button type="submit" class="btn btn-outline-primary">
						<i class="fa fa-save"></i> Guardar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!--==========================
MODAL EDITAR TEMA
============================-->
<div class="modal fade" id="modalEditarTema" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?= base_url('Temas/update') ?>" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<i class="fa fa-plus-circle"></i>
						Editar Tema
					</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="editarTema">Tema:</label>
						<input type="hidden" name="idTema" id="idTema">
						<input type="hidden" name="idHeader" id="idHeader">
						<input type="text" class="form-control" id="editarTema"
							   name="tema"
							   placeholder="Escriba el titulo del slider" required>
					</div>
					<div class="form-group">
						<label for="editarSlug">Slug:</label>
						<input type="text" class="form-control" id="editarSlug"
							   name="slug" readonly>
					</div>
					<div class="form-group">
						<label for="editarDescripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion" rows="3"
								  id="editarDescripcion" placeholder="Escriba la descripción"></textarea>
					</div>
					<div class="form-group my-2 text-center">
						<div class="btn btn-default btn-file mb-3">
							<i class="fas fa-paperclip"></i> Adjuntar Imagen de Portada
							<input type="file" name="portada" id="editarPortada" class="portada">
							<input type="hidden" name="portadaActual" id="portadaActual">
						</div>
						<br>
						<img src="" alt="Portada"
							 class="img-fluid py-2 prevPortada">
						<p class="help-block small mt-3">Dimensiones: 700px * 420px |
							Peso Max. 2MB | Formato: JPG o PNG</p>
						<div class="custom-control custom-switch">
							<input class="custom-control-input" type="checkbox"
								   name="resizePortada" id="resizePortadaE" value="Si">
							<label for="resizePortadaE"
								   class="custom-control-label">Forzar
								Redimensionado de Imagen</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
						<i class="fas fa-times-circle"></i> Cerrar
					</button>
					<button type="submit" class="btn btn-outline-primary">
						<i class="fa fa-save"></i> Guardar Cambios
					</button>
				</div>
			</div>
		</form>
	</div>
</div>


<script type="text/javascript" src="<?= base_url('public/js/temas.js') ?>"></script>


