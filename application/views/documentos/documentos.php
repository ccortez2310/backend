<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Documentos
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Inicio</a></li>
                        <li class="breadcrumb-item active">Documentos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h5 class="mt-1 float-left">
                        <i class="nav-icon fas fa-archive"></i> Todos los documentos
                    </h5>
                    <button class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#modalNuevoDoc">
                        <i class="fas fa-plus-circle"></i> Nuevo Documento
                    </button>
                </div>
                <div class="card-body">
                    <table class="table table-hover dt-responsive  tablaDocs">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th>Descripción</th>
                                <th>Peso</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($docs as $key => $value) :
                                if ($value->estado == 0) {
                                    $color = "btn-danger";
                                    $estado = 1;
                                    $texto = "Inhabilitado";
                                } else {
                                    $color = "btn-success";
                                    $estado = 0;
                                    $texto = "Activo";
                                }

                                $btnEstado = '<button type="button" 
												  class="btn ' . $color . ' btn-xs btnActivar"
												  idDoc = "' . $value->id . '" 
												  estadoDoc="' . $estado . '">' . $texto . '</button>';
                            ?>
                                <tr>
                                    <td><?php echo ($key + 1) ?></td>
                                    <td><a href="<?= base_url($value->path) ?>">
                                            <?php echo $value->titulo ?>
                                        </a> </td>
                                    <td><?php echo $value->descripcion ?></td>
                                    <td><?php echo $value->peso ?></td>
                                    <td><?php echo $value->fecha ?></td>
                                    <td><?php echo $btnEstado ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-warning text-white btnEditarDoc" idDoc="<?php echo $value->id ?>" title="Editar" data-toggle="modal" data-target="#modalEditarSlider">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger btnEliminarDoc" 
                                            idDoc="<?php echo $value->id ?>" 
                                            docFile="<?php echo $value->path ?>"
                                            title="Eliminar">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--==========================
MODAL NUEVO DOC
============================-->
<div class="modal fade" id="modalNuevoDoc" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form action="<?= base_url('Documentos/saveDoc') ?>" method="post" enctype="multipart/form-data" id="newDocForm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class="fa fa-plus-circle"></i>
                        Agregar Documento
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="titulo">Título:</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Puede renombrar el nombre del documento aquí" required>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción:</label>
                        <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Escriba la descripción del documento"></textarea>
                    </div>
                    <div class="form-group my-2 text-center">
                        <div class="btn btn-primary btn-file">
                            <i class="fas fa-paperclip"></i> Adjuntar documento
                            <input type="file" name="docFile" id="docFile" class="docFile" required>
                        </div>
                        <br>
                        <p class="help-block small mt-3">
                            Tamaño máximo permitido: 2MB. |
                            Formato: PDF, DOCX (WORD), PNG, PPT(POWER POINT), XLS (EXCEL)
                        </p>
                    </div>
                    <input type="text" hidden name="peso" id="peso">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
                        <i class="fas fa-times-circle"></i> Cancelar
                    </button>
                    <button type="submit" class="btn btn-outline-primary">
                        <i class="fa fa-check"></i> Guardar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?= base_url('public/js/documentos.js') ?>"></script>