<?php if ($this->session->flashdata('success')): ?>
	<script type="text/javascript">
		//toastr.success("<?//php //echo $this->session->flashdata("success")?>//");
		notie.alert({
			type: 1,
			text: "<?php echo $this->session->flashdata("success") ?>",
			time: 3
		})
	</script>
<?php endif; ?>
<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Sliders</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url() ?>">Inicio</a></li>
						<li class="breadcrumb-item active">Sliders</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header ">
					<h5 class="mt-1 float-left">
						<i class="fas fa-images"></i> Todos los Sliders
					</h5>
					<button class="btn btn-outline-primary float-right" data-toggle="modal"
							data-target="#modalNuevoSlider">
						<i class="fas fa-plus-circle"></i> Nuevo Slider
					</button>
				</div>
				<div class="card-body">
					<table class="table table-hover dt-responsive dtes tablaSlider">
						<thead>
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripción</th>
							<th>Imagen</th>
							<th>Estado</th>
							<th>Acciones</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($sliders as $key => $value):

							if ($value->estado == 0) {
								$color = "btn-danger";
								$estado = 1;
								$texto = "Desactivado";
							} else {
								$color = "btn-success";
								$estado = 0;
								$texto = "Activado";
							}

							$btnEstado = '<button type="button" 
												  class="btn ' . $color . ' btn-xs btnActivar"
												  idSlider = "' . $value->id . '" 
												  estadoSlider="' . $estado . '">' . $texto . '</button>';

							?>
							<tr>
								<td><?php echo($key + 1) ?></td>
								<td><?php echo $value->titulo ?></td>
								<td><?php echo $value->descripcion ?></td>
								<td>
									<a data-toggle="modal" data-target="#slide<?php echo $value->id ?>">
										<img src="<?php echo $value->portada ?>"
											 alt="<?php echo $value->titulo ?>"
											 class="img-thumbnail" width="100px">
									</a>
									<div class="modal fade" id="slide<?php echo $value->id ?>" tabindex="-1"
										 role="dialog">
										<button type="button" class="close mr-3 text-white" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
											<img src="<?php echo $value->portada ?>"
												 alt="<?php echo $value->titulo ?>"
												 class="img-fluid rounded">
										</div>
									</div>
								</td>
								<td><?php echo $btnEstado ?></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-warning text-white btnEditarSlider"
												idSlider="<?php echo $value->id ?>"
												title="Editar"
												data-toggle="modal"
												data-target="#modalEditarSlider">
											<i class="fas fa-edit"></i>
										</button>
										<button type="button" class="btn btn-danger btnEliminarSlider"
												idSlider="<?php echo $value->id ?>"
												imgSlider="<?php echo $value->portada ?>"
												title="Eliminar">
											<i class="fas fa-trash"></i>
										</button>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--==========================
MODAL NUEVO SLIDER
============================-->
<div class="modal fade" id="modalNuevoSlider" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?= base_url('Slider/save') ?>" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<i class="fa fa-plus-circle"></i>
						Agregar Slider
					</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="titulo">Titulo:</label>
						<input type="text" class="form-control" id="titulo"
							   name="titulo"
							   placeholder="Escriba el titulo del slider" required>
					</div>
					<div class="form-group">
						<label for="descripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion"
								  id="descripcion" placeholder="Escriba la descripción"></textarea>
					</div>
					<div class="form-group my-2 text-center">
						<div class="btn btn-default btn-file mb-3">
							<i class="fas fa-paperclip"></i> Adjuntar Imagen del Slider
							<input type="file" name="portada" id="portada" class="portada" required>
						</div>
						<br>
						<img src="<?= base_url() . 'uploads/sliders/default/default.jpg' ?>" alt="Portada"
							 class="img-fluid py-2 prevPortada">
						<p class="help-block small mt-3">Dimensiones: 1350px * 700px |
							Peso Max. 2MB | Formato: JPG o PNG</p>
						<div class="custom-control custom-switch">
							<input class="custom-control-input" type="checkbox"
								   name="resizePortada" id="resizePortada" value="Si">
							<label for="resizePortada"
								   class="custom-control-label">Forzar
								Redimensionado de Imagen</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
						<i class="fas fa-times-circle"></i> Cerrar
					</button>
					<button type="submit" class="btn btn-outline-primary">
						<i class="fa fa-save"></i> Guardar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!--==========================
MODAL EDITAR SLIDER
============================-->
<div class="modal fade" id="modalEditarSlider" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form action="<?= base_url('Slider/update') ?>" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<i class="fa fa-plus-circle"></i>
						Editar Datos del Slider
					</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="titulo">Titulo:</label>
						<input type="hidden" name="idSlider" id="idSlider">
						<input type="text" class="form-control" id="titulo"
							   name="titulo"
							   placeholder="Escriba el titulo del slider" required>
					</div>
					<div class="form-group">
						<label for="descripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion"
								  id="descripcion" placeholder="Escriba la descripción"></textarea>
					</div>
					<div class="form-group my-2 text-center">
						<div class="btn btn-default btn-file mb-3">
							<i class="fas fa-paperclip"></i> Adjuntar Imagen Slider
							<input type="file" name="portada" id="portada" class="portada">
							<input type="hidden" name="portadaActual" id="portadaActual">
						</div>
						<br>
						<img src="" alt="Portada"
							 class="img-fluid py-2 prevPortada">
						<p class="help-block small mt-3">Dimensiones: 1350px * 700px |
							Peso Max. 2MB | Formato: JPG o PNG</p>
						<div class="custom-control custom-switch">
							<input class="custom-control-input" type="checkbox"
								   name="resizePortada" id="resizePortadaE" value="Si">
							<label for="resizePortadaE"
								   class="custom-control-label">Forzar
								Redimensionado de Imagen</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
						<i class="fas fa-times-circle"></i> Cerrar
					</button>
					<button type="submit" class="btn btn-outline-primary">
						<i class="fa fa-save"></i> Guardar Cambios
					</button>
				</div>
			</div>
		</form>
	</div>
</div>


<script type="text/javascript" src="<?= base_url('public/js/slider.js') ?>"></script>

