$(".docFile").change(function () {

    let documento = this.files[0];

	/*=============================================
	  VALIDAR EL FORMATO DE LA DOC SEA WORD, PDF O PNG
	  =============================================*/
    if (
        documento["type"] != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" 
        && documento["type"] != "image/png" 
        && documento["type"] != "application/pdf"
        && documento["type"] != "vnd.ms-excel"
        && documento["type"] != "application/vnd.ms-powerpoint") {
        $(".docFile").val("");
        swal({
            title: "Error al procesar el archivo:",
            text: "Debe cumplir con los formatos especificados",
            type: "error",
            confirmButtonText: "Cerrar"
        });

    } else if (documento["size"] > 2000000) {
        $(".docFile").val("");
        swal({
            title: "Error al procesar el archivo:",
            text: "No debe pesar más de 2MB",
            type: "error",
            confirmButtonText: "Cerrar"
        });
    } else {
        let datosDoc = new FileReader;
        datosDoc.readAsDataURL(documento);
    }

});

/*=============================================
ELIMINAR DOCUMENTO
=============================================*/
$(".tablaDocs tbody").on("click", ".btnEliminarDoc", function () {

    let idDoc = $(this).attr("idDoc");
    let docFile = $(this).attr("docFile");

    let data = new FormData();
    data.append("idDoc", idDoc);
    data.append("docFile", docFile);

    swal({
        title: '¿Desea eliminar el documento?',
        text: "Si no lo está, puede cancelar la acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Sí, borrar documento'
    }).then(function (result) {

        if (result.value) {

            $.ajax({
                url: urlAjax + "Documentos/deleteDoc",
                method: "POST",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (respuesta) {
                    if (respuesta == "ok") {
                        swal({
                            type: "success",
                            title: "El documento ha sido eliminado",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        }).then(function (result) {
                            if (result.value) {
                                window.location = "documentos";
                            }
                        })
                    }
                }
            })
        }
    })
});

