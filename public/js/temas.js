/*=================
ACTIVAR BANNER
===================*/

$(".tablaTemas tbody").on("click", ".btnActivar", function () {

	let idTema = $(this).attr("idTema");
	let estadoTema = $(this).attr("estadoTema");

	let datos = new FormData();
	datos.append("idTema", idTema);
	datos.append("estadoTema", estadoTema);

	$.ajax({
		url: urlAjax + "Temas/changeState",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success: function (respuesta) {
			console.log("respuesta", respuesta);
		}
	});

	if (estadoTema == 0) {
		$(this).removeClass('btn-success');
		$(this).addClass('btn-danger');
		$(this).html('Desactivado');
		$(this).attr('estadoTema', 1);
	} else {
		$(this).addClass('btn-success');
		$(this).removeClass('btn-danger');
		$(this).html('Activado');
		$(this).attr('estadoTema', 0);
	}

});

/*================================
SUBIR PORTADA
==================================*/

$(".portada").change(function () {

	let imagen = this.files[0];

	/*=============================================
	  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
	  =============================================*/
	if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {
		$(".portada").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen debe estar en formato JPG o PNG!",
			type: "error",
			confirmButtonText: "¡Cerrar!"
		});

	} else if (imagen["size"] > 2000000) {
		$(".portada").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen no debe pesar más de 2MB!",
			type: "error",
			confirmButtonText: "¡Cerrar!"
		});
	} else {
		let datosImagen = new FileReader;
		datosImagen.readAsDataURL(imagen);

		$(datosImagen).on("load", function (event) {
			let rutaImagen = event.target.result;
			$(".prevPortada").attr("src", rutaImagen);

		})
	}

});

/*=============================================
EDITAR TEMA
=============================================*/

$(".tablaTemas tbody").on("click", ".btnEditarTema", function () {

	let idTema = $(this).attr("idTema");
	let datos = new FormData();
	datos.append("idTema", idTema);

	$.ajax({
		url: urlAjax + "Temas/getTemaByID",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function (respuesta) {

			$("#idTema").val(respuesta["id"]);
			$("#idHeader").val(respuesta["idHeader"]);
			$("#editarTema").val(respuesta["tema"]);
			$("#editarSlug").val(respuesta["slug"]);
			$("#editarDescripcion").val(respuesta["descripcion"]);
			$("#modalEditarTema .prevPortada").attr("src", respuesta["portada"]);
			$("#portadaActual").val(respuesta["portada"]);

		}

	})

});

/*=============================================
ELIMINAR TEMA
=============================================*/
$(".tablaTemas tbody").on("click", ".btnEliminarTema", function () {

	let idTema = $(this).attr("idTema");
	let imgTema = $(this).attr("imgTema");
	let slug = $(this).attr("slug");

	let data = new FormData();
	data.append("idTema", idTema);
	data.append("imgTema", imgTema);
	data.append("slug", slug);

	swal({
		title: '¿Está seguro de borrar el Tema?',
		text: "¡Si no lo está puede cancelar la accíón!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si, borrar Tema!'
	}).then(function (result) {

		if (result.value) {

			$.ajax({
				url: urlAjax + "Temas/delete",
				method: "POST",
				data: data,
				cache: false,
				contentType: false,
				processData: false,
				dataType: "json",
				success: function (respuesta) {
					if (respuesta == "ok") {
						swal({
							type: "success",
							title: "El Tema ha sido borrado correctamente",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
						}).then(function (result) {
							if (result.value) {
								window.location = "temas";
							}
						})
					}
				}
			})
		}
	})
});

function limpiarUrl(text) {
	let texto = text.toLowerCase();
	texto = texto.replace(/[á]/g, 'a');
	texto = texto.replace(/[é]/g, 'e');
	texto = texto.replace(/[í]/g, 'i');
	texto = texto.replace(/[ó]/g, 'o');
	texto = texto.replace(/[ú]/g, 'u');
	texto = texto.replace(/[ñ]/g, 'n');
	texto = texto.replace(/ /g, '-');
	return texto;
}

$("#tema").on("keyup", function () {
	$("#slug").val(limpiarUrl($("#tema").val()));
});

$("#editarTema").on("keyup", function () {
	$("#editarSlug").val(limpiarUrl($("#editarTema").val()));
});
