/*=================
ACTIVAR BANNER
===================*/

$(".tablaSlider tbody").on("click", ".btnActivar", function () {

	let idSlider = $(this).attr("idSlider");
	let estadoSlider = $(this).attr("estadoSlider");

	let datos = new FormData();
	datos.append("idSlider", idSlider);
	datos.append("estadoSlider", estadoSlider);

	$.ajax({
		url: urlAjax + "slider/changeState",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		success: function (respuesta) {
			console.log("respuesta", respuesta);
		}
	});

	if (estadoSlider == 0) {
		$(this).removeClass('btn-success');
		$(this).addClass('btn-danger');
		$(this).html('Desactivado');
		$(this).attr('estadoSlider', 1);
	} else {
		$(this).addClass('btn-success');
		$(this).removeClass('btn-danger');
		$(this).html('Activado');
		$(this).attr('estadoSlider', 0);
	}

});

/*================================
SUBIR SLIDER
==================================*/

$(".portada").change(function () {

	let imagen = this.files[0];

	/*=============================================
	  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
	  =============================================*/
	if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {
		$(".portada").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen debe estar en formato JPG o PNG!",
			type: "error",
			confirmButtonText: "¡Cerrar!"
		});

	} else if (imagen["size"] > 2000000) {
		$(".portada").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen no debe pesar más de 2MB!",
			type: "error",
			confirmButtonText: "¡Cerrar!"
		});
	} else {
		let datosImagen = new FileReader;
		datosImagen.readAsDataURL(imagen);

		$(datosImagen).on("load", function (event) {
			let rutaImagen = event.target.result;
			$(".prevPortada").attr("src", rutaImagen);

		})
	}

});

/*=============================================
EDITAR SLIDER
=============================================*/

$(".tablaSlider tbody").on("click", ".btnEditarSlider", function () {

	let idSlider = $(this).attr("idSlider");
	let datos = new FormData();
	datos.append("idSlider", idSlider);

	$.ajax({
		url: urlAjax + "Slider/getSliderByID",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function (respuesta) {

			$("#modalEditarSlider #idSlider").val(respuesta["id"]);
			$("#modalEditarSlider #titulo").val(respuesta["titulo"]);
			$("#modalEditarSlider #descripcion").val(respuesta["descripcion"]);
			$("#modalEditarSlider .prevPortada").attr("src", respuesta["portada"]);
			$("#modalEditarSlider #portadaActual").val(respuesta["portada"]);

		}

	})

});

/*=============================================
ELIMINAR SLIDER
=============================================*/
$(".tablaSlider tbody").on("click", ".btnEliminarSlider", function () {

	let idSlider = $(this).attr("idSlider");
	let imgSlider = $(this).attr("imgSlider");

	let data = new FormData();
	data.append("idSlider", idSlider);
	data.append("imgSlider", imgSlider);

	swal({
		title: '¿Está seguro de borrar el Slider?',
		text: "¡Si no lo está puede cancelar la accíón!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si, borrar slider!'
	}).then(function (result) {

		if (result.value) {

			$.ajax({
				url: urlAjax + "Slider/delete",
				method: "POST",
				data: data,
				cache: false,
				contentType: false,
				processData: false,
				dataType: "json",
				success: function (respuesta) {
					if (respuesta == "ok") {
						swal({
							type: "success",
							title: "El Slider ha sido borrado correctamente",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
						}).then(function (result) {
							if (result.value) {
								window.location = "sliders";
							}
						})
					}
				}
			})
		}
	})
});

