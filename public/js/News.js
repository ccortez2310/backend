$(document).ready(function () {

	$("img").addClass("img-fluid");

	/*=================
	ACTIVAR BANNER
	===================*/

	$(".tablaNoticias tbody").on("click", ".btnActivar", function () {

		let idNew = $(this).attr("idNew");
		let estadoNew = $(this).attr("estadoNew");

		let datos = new FormData();
		datos.append("idNew", idNew);
		datos.append("estadoNew", estadoNew);

		$.ajax({
			url: urlAjax + "news/changeState",
			method: "POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			success: function (respuesta) {
				console.log("respuesta", respuesta);
			}
		});

		if (estadoNew == 0) {
			$(this).removeClass('btn-success');
			$(this).addClass('btn-danger');
			$(this).html('No publicada');
			$(this).attr('estadoNew', 1);
		} else {
			$(this).addClass('btn-success');
			$(this).removeClass('btn-danger');
			$(this).html('Publicada');
			$(this).attr('estadoNew', 0);
		}

	});

	/*================================
	SUBIR PORTADA
	==================================*/

	$(".imgPortada").change(function () {

		let imagen = this.files[0];

		/*=============================================
		  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
		  =============================================*/
		if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {
			$(".imgSlider").val("");
			swal({
				title: "Error al subir la imagen",
				text: "¡La imagen debe estar en formato JPG o PNG!",
				type: "error",
				confirmButtonText: "¡Cerrar!"
			});

		} else if (imagen["size"] > 2000000) {
			$(".imgSlider").val("");
			swal({
				title: "Error al subir la imagen",
				text: "¡La imagen no debe pesar más de 2MB!",
				type: "error",
				confirmButtonText: "¡Cerrar!"
			});
		} else {
			let datosImagen = new FileReader;
			datosImagen.readAsDataURL(imagen);

			$(datosImagen).on("load", function (event) {
				let rutaImagen = event.target.result;
				$(".prevPortada").attr("src", rutaImagen);

			})
		}

	});

	$('#contenido').summernote({
		height: "400px",
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'video', 'hr']],
			['view', ['fullscreen', 'codeview']],
		],
		callbacks: {
			onImageUpload: function (image) {
				uploadImage(image[0]);
			},
			onMediaDelete: function (target) {
				deleteImage(target[0].src);
			}
		}
	});

	function uploadImage(image) {
		let data = new FormData();
		data.append("image", image);
		$.ajax({
			url: urlAjax + "news/uploadImage",
			cache: false,
			contentType: false,
			processData: false,
			data: data,
			type: "POST",
			success: function (url) {
				$('#contenido').summernote("insertImage", url, function ($image) {
					$image.attr("class", "img-fluid");
				});
			},
			error: function (data) {
				console.log(data);
			}
		});
	}

	function deleteImage(src) {
		$.ajax({
			data: {src: src},
			type: "POST",
			url: urlAjax + "news/deleteImage",
			cache: false,
			success: function (response) {
				console.log(response);
			}
		});
	}

	function limpiarUrl(text) {
		let texto = text.toLowerCase();
		texto = texto.replace(/[á]/g, 'a');
		texto = texto.replace(/[é]/g, 'e');
		texto = texto.replace(/[í]/g, 'i');
		texto = texto.replace(/[ó]/g, 'o');
		texto = texto.replace(/[ú]/g, 'u');
		texto = texto.replace(/[ñ]/g, 'n');
		texto = texto.replace(/ /g, '-');
		return texto;
	}

	$(".tituloNoticia").on("keyup", function () {
		$(".rutaNoticia").val(limpiarUrl($(".tituloNoticia").val()));
	});


	/*====================
	ELIMINAR NOTICIA
	======================*/
	$(".tablaNoticias tbody").on("click", ".btnEliminarNew", function () {

		let idNew = $(this).attr("idNew");
		let ruta = $(this).attr("ruta");

		let data = new FormData();
		data.append("idNew", idNew);
		data.append("ruta", ruta);

		swal({
			title: '¿Está seguro de borrar la noticia?',
			text: "¡Si no lo está puede cancelar la accíón!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: 'Cancelar',
			confirmButtonText: 'Si, borrar noticia!'
		}).then(function (result) {

			if (result.value) {

				$.ajax({
					url: urlAjax + "news/delete",
					method: "POST",
					data: data,
					cache: false,
					contentType: false,
					processData: false,
					dataType: "json",
					success: function (respuesta) {
						if (respuesta == "ok") {
							swal({
								type: "success",
								title: "La noticia ha sido borrado correctamente",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"
							}).then(function (result) {
								if (result.value) {
									window.location = "news";
								}
							})
						}
					}
				})
			}
		})
	});


});
