$(document).ready(function () {

	/*=================
	ACTIVAR AREA
	===================*/

	$(".tablaPA tbody").on("click", ".btnActivar", function () {

		let idPA = $(this).attr("idPA");
		let estadoPA = $(this).attr("estadoPA");

		let datos = new FormData();
		datos.append("idPA", idPA);
		datos.append("estadoPA", estadoPA);

		$.ajax({
			url: urlAjax + "practiceareas/changeState",
			method: "POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			success: function (respuesta) {
				console.log("respuesta", respuesta);
			}
		});

		if (estadoPA == 0) {
			$(this).removeClass('btn-success');
			$(this).addClass('btn-danger');
			$(this).html('Inactiva');
			$(this).attr('estadoPA', 1);
		} else {
			$(this).addClass('btn-success');
			$(this).removeClass('btn-danger');
			$(this).html('Activa');
			$(this).attr('estadoPA', 0);
		}

	});

	/*================================
	SUBIR PORTADA
	==================================*/

	$(".imgPortada").change(function () {

		let imagen = this.files[0];

		/*=============================================
		  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
		  =============================================*/
		if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {
			$(".imgSlider").val("");
			swal({
				title: "Error al subir la imagen",
				text: "¡La imagen debe estar en formato JPG o PNG!",
				type: "error",
				confirmButtonText: "¡Cerrar!"
			});

		} else if (imagen["size"] > 2000000) {
			$(".imgSlider").val("");
			swal({
				title: "Error al subir la imagen",
				text: "¡La imagen no debe pesar más de 2MB!",
				type: "error",
				confirmButtonText: "¡Cerrar!"
			});
		} else {
			let datosImagen = new FileReader;
			datosImagen.readAsDataURL(imagen);

			$(datosImagen).on("load", function (event) {
				let rutaImagen = event.target.result;
				$(".prevPortada").attr("src", rutaImagen);

			})
		}

	});

	$('#descripcion').summernote({
		height: "300px",
		toolbar: [
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['para', ['ul', 'ol', 'paragraph']],
			['view', ['fullscreen', 'codeview']],
		]
	});

	function limpiarUrl(text) {
		let texto = text.toLowerCase();
		texto = texto.replace(/[á]/g, 'a');
		texto = texto.replace(/[é]/g, 'e');
		texto = texto.replace(/[í]/g, 'i');
		texto = texto.replace(/[ó]/g, 'o');
		texto = texto.replace(/[ú]/g, 'u');
		texto = texto.replace(/[ñ]/g, 'n');
		texto = texto.replace(/ /g, '-');
		return texto;
	}

	$(".tituloAP").on("keyup", function () {
		$(".rutaAP").val(limpiarUrl($(".tituloAP").val()));
	});


	/*====================
	ELIMINAR AREA DE PRACTICA
	======================*/
	$(".tablaPA tbody").on("click", ".btnEliminar", function () {

		let idPA = $(this).attr("idPA");
		let portada = $(this).attr("portada");

		let data = new FormData();
		data.append("idPA", idPA);
		data.append("portada", portada);

		swal({
			title: '¿Está seguro de borrar el área de práctica?',
			text: "¡Si no lo está puede cancelar la accíón!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: 'Cancelar',
			confirmButtonText: 'Si, borrar!'
		}).then(function (result) {
			if (result.value) {
				$.ajax({
					url: urlAjax + "practiceareas/delete",
					method: "POST",
					data: data,
					cache: false,
					contentType: false,
					processData: false,
					dataType: "json",
					success: function (respuesta) {
						if (respuesta == "ok") {
							swal({
								type: "success",
								title: "El Registro ha sido borrado correctamente",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"
							}).then(function (result) {
								if (result.value) {
									window.location = "practiceareas";
								}
							})
						}
					}
				})
			}
		})
	});


});
