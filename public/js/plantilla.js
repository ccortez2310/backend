
const urlAjax = "http://localhost/backend/";

//Datepicker
$('.datepicker').datepicker({
	format: 'dd/mm/yyyy'
});

let datetime = null;
let dateday = null;
let datedays = null;
date = null;

const update = function () {
	const date = moment(new Date());
	dateday.html(date.format('dddd'));
	datedays.html(date.format('Do MMM'));
	datetime.html(date.format('h:mm A'));
};

moment.locale('es');

datetime = $('#datetime');
dateday = $('#dateday');
datedays = $('#datedays');
update();
setInterval(update, 1000);


/*=============================================
ACTIVAR SIDEBAR
=============================================*/
$(document).ready(function (e) {
	let url = window.location.href;
	let i = url.split('/');
	url = i[4];
	$('a[href="'+ urlAjax + url + '"]').addClass('active');
});

toastr.options = {
	"closeButton": true,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-top-right",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "2000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}

$(".dt-responsive").css('width', '100%');

$(".preloader").fadeOut();
