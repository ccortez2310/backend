
/*=============================================
AGREGAR RED SOCIAL
=============================================*/

$(document).on("click", ".agregarRed", function (e) {

	e.preventDefault();

	let url = $("#url_red").val();
	let icono = $("#icono_red").val().split(",")[0];
	let color = $("#icono_red").val().split(",")[1];

	let listaRedes = JSON.parse($("#listaRed").val());

	let element = {url: url, icono: icono, color: color};

	let existe = false;

	for (let red of listaRedes) {
		if ((red.url == element.url) || (red.icono == element.icono)) {
			existe = true;
			toastr["warning"]("Esta red social ya ha sido agregada");
		}
	}

	if (!existe) {
		listaRedes.push(element);
		$(".listadoRedes").append(`
			<div class="col-12">
				<div class="callout callout-indigo">
					<div class="row">
						<div class="col-11 d-flex align-items-center">
							<a type="button" style="color: ${color}">
								<i class="${icono}"></i>
							</a>
							<span class="ml-md-2 ml-2 ml-sm-2"> ${url} </span>
						</div>
						<div class="col-1">
							<button type="button" class="btn btn-outline-danger btn-xs eliminarArea"
								red="${icono}" url="${url}" title="Haga click para eliminar">
								<i class="fas fa-trash"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		`);
		toastr["success"]("La red social ha sido agregada");
	}

	$("#listaRed").val(JSON.stringify(listaRedes));
	$("#url_red").val("");

});

/*=============================================
ELIMINAR RED SOCIAL
=============================================*/
$(document).on("click", ".eliminarRed", function (e) {

	e.preventDefault();

	let red = $(this).attr("red");
	let url = $(this).attr("url");

	let listaRed = JSON.parse($("#listaRed").val());

	let element = {red: red, url: url};

	for (let i = 0; i < listaRed.length; i++) {
		if ((listaRed[i].url == element.url) && (listaRed[i].icono == element.red)) {
			listaRed.splice(i, 1);
			$(this).parent().parent().parent().parent().remove();
			$("#listaRed").val(JSON.stringify(listaRed));
			toastr["success"]("La red ha sido eliminada");
		}
	}
});

/*================================
SUBIR IMG ICONO
==================================*/

$("#icono").change(function () {

	let imagen = this.files[0];

	/*=============================================
	  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
	  =============================================*/
	if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" && imagen["type"] != "image/x-icon" ) {
		$("#icono").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen debe estar en formato JPG o PNG o ICO!",
			type: "error",
			confirmButtonText: "¡Ok!"
		});

	} else if (imagen["size"] > 2000000) {
		$("#icono").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen no debe pesar más de 2 MB!",
			type: "error",
			confirmButtonText: "¡Ok!"
		});
	} else {
		let datosImagen = new FileReader;
		datosImagen.readAsDataURL(imagen);

		$(datosImagen).on("load", function (event) {
			let rutaImagen = event.target.result;
			$(".prevIcono").attr("src", rutaImagen);

		})
	}

});

/*================================
SUBIR IMG LOGO
==================================*/

$("#logo").change(function () {

	let imagen = this.files[0];

	/*=============================================
	  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
	  =============================================*/
	if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" ) {
		$("#logo").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen debe estar en formato JPG o PNG!",
			type: "error",
			confirmButtonText: "¡Ok!"
		});

	} else if (imagen["size"] > 2000000) {
		$("#logo").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen no debe pesar más de 2 MB!",
			type: "error",
			confirmButtonText: "¡Cerrar!"
		});
	} else {
		let datosImagen = new FileReader;
		datosImagen.readAsDataURL(imagen);

		$(datosImagen).on("load", function (event) {
			let rutaImagen = event.target.result;
			$(".prevLogo").attr("src", rutaImagen);

		})
	}

});

/*================================
SUBIR IMG PORTADA
==================================*/

$("#portada").change(function () {

	let imagen = this.files[0];

	/*=============================================
	  VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
	  =============================================*/
	if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" ) {
		$("#portada").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen debe estar en formato JPG o PNG!",
			type: "error",
			confirmButtonText: "¡Ok!"
		});

	} else if (imagen["size"] > 2000000) {
		$("#portada").val("");
		swal({
			title: "Error al subir la imagen",
			text: "¡La imagen no debe pesar más de 2 MB!",
			type: "error",
			confirmButtonText: "¡Cerrar!"
		});
	} else {
		let datosImagen = new FileReader;
		datosImagen.readAsDataURL(imagen);

		$(datosImagen).on("load", function (event) {
			let rutaImagen = event.target.result;
			$(".prevPortada").attr("src", rutaImagen);

		})
	}

});
